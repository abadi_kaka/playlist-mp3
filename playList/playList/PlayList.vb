﻿Imports System.IO

Public Class PlayList

    Dim i As Integer = 0
    Public Property Multiselect As Boolean
    Dim sortColumn As Integer = -1
    Dim CurrentMp3 As Mp3Class
    Dim trak As mcTrack
    Dim LItem As ListViewItem
    Dim gerak As String = ""
    Dim namafile As String
    Dim gerak2 As String
    Dim cek As Integer = -1
    Dim state As Boolean = False
    Dim lvIndex As System.Int32

    Dim Index As System.Int32
    Dim bolShuf As Boolean = False
    Dim prev As Boolean = False
    Dim rev As Boolean = False
    Dim flag As Boolean = false
    Dim stringPrev As Integer = -1
    Dim stringSebelumnya As String

    '--------------------- variable property --------------------------------------------------
    'Private backgroundmove As Color = Color.WhiteSmoke
    'Private foregroundmove As Color = Color.Red
    ''Private fontmove As Font = New Font("Lucida Sans Unicode", 8, FontStyle.Bold)
    Private setTimer As Integer = 100

    Enum formSize
        SMALL
        MEDIUM
        LARGE
    End Enum

    Private sizePlaylist As formSize = formSize.small


    Private Sub UserControl1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListView1.BackColor = Color.White
        ListView1.BorderStyle = Windows.Forms.BorderStyle.FixedSingle
        ListView1.MultiSelect = True
        ListView1.FullRowSelect = True

        'small() 356
        small()
        ' awal sekali
        'ListView1.Columns(0).Width = 28
        'ListView1.Columns(1).Width = 250
        'ListView1.Columns(2).Width = 100
        'ListView1.Columns(3).Width = 70
        'ListView1.Columns(4).Width = 0



        Timer1.Enabled = False
        Timer1.Interval = 100
    End Sub

    '--------------------------------- add ------------------------------------------------------------------------
    Private Sub btAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAdd.Click
        GetFiles()
    End Sub

    Private Sub GetFiles()
        i = ListView1.Items.Count
        ListView1.BeginUpdate()
        Me.OpenFileDialog1.Multiselect = True
        Dim dr As DialogResult = Me.OpenFileDialog1.ShowDialog()
        If (dr = System.Windows.Forms.DialogResult.OK) Then
            Dim file As String
            For Each file In OpenFileDialog1.FileNames
                Dim sExtension As String = Path.GetExtension(file)
                sExtension = sExtension.ToLower
                If (Trim(sExtension) = ".mp3" Or Trim(sExtension) = ".wav") = True Then
                    LItem = New ListViewItem()
                    CurrentMp3 = New Mp3Class(file) ' dari class id3v1

                    'kalau pake class 1 artis gk baca dg benar mknyA pake class 2
                    trak = New mcTrack() 'class id3v2
                    trak = mfcReadID3v2Tag.ReadID3v2Tag(file)
                    i = i + 1
                    With LItem
                        .SubItems(0).Text = i.ToString
                        .SubItems.Add(Path.GetFileNameWithoutExtension(file))
                        .SubItems.Add(trak.Artist)
                        .SubItems.Add(CurrentMp3.GetDurationString)

                        .SubItems.Add(file)

                    End With
                    ListView1.Items.Add(LItem)
                End If
            Next file
        End If
        ListView1.EndUpdate()
        ListView1.Columns(0).Width = -2
    End Sub

    '-------------------------------- remove --------------------------------------------------------------------
    Private Sub btRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRemove.Click
        copot()

    End Sub

    Function copot()

        Dim result As DialogResult
        result = MessageBox.Show("Are you sure ?", "Remove", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
        If (result = DialogResult.Yes) Then

            Dim LItem As ListViewItem
            Dim count As Integer
            For Each x As ListViewItem In ListView1.SelectedItems

                ListView1.Items.Remove(x)
                i -= 1
            Next

            For i = 0 To ListView1.Items.Count - 1
                ListView1.Items(i).Text = i + 1.ToString
            Next
        End If

    End Function

    ' ------------------------------ sort -----------------------------------------------------------------------
    Private Sub listView1_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles ListView1.ColumnClick
        Try

            ListView1.Items(cek).SubItems(1).Text = stringSebelumnya

        Catch ex As Exception
        End Try

        cek = -1
        flag = True
        If flag = True Then
            If e.Column <> sortColumn Then
                ' Set the sort column to the new column.
                sortColumn = e.Column
                ' Set the sort order to ascending by default.
                ListView1.Sorting = SortOrder.Ascending
            Else
                ' Determine what the last sort order was and change it.
                If ListView1.Sorting = SortOrder.Ascending Then
                    ListView1.Sorting = SortOrder.Descending
                Else
                    ListView1.Sorting = SortOrder.Ascending
                End If
            End If
            ' Call the sort method to manually sort.
            ListView1.Sort()
            ' Set the ListViewItemSorter property to a new ListViewItemComparer
            ' object.
            ListView1.ListViewItemSorter = New ListViewItemComparer(e.Column, _
                                                            ListView1.Sorting)


            For i = 0 To ListView1.Items.Count - 1
                ListView1.Items(i).Text = i + 1.ToString

            Next

        End If
    End Sub

    '------------------------------- tulisan gerak --------------------------------------------------------------
    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        

        '--------------
        If flag = False Then
            If state = False Then
                cek = ListView1.SelectedItems.Item(0).Text - 1
                state = True
                gerak = ListView1.Items(cek).SubItems(1).Text
            End If

            '' --- error saat delete , kasih trycacth disini ---------------
            Try
                ListView1.Items(cek).SubItems(1).Text = gerak
            Catch ex As Exception
            End Try
            '----------------------'


            '----------------------'
            Timer1.Enabled = True
            For Each x As ListViewItem In ListView1.SelectedItems
                gerak = x.SubItems(1).Text
                cek = x.Index
            Next
            gerak2 = "     " + gerak + "                                                 "
            stringSebelumnya = gerak
        End If

    End Sub

    Private Sub ListView1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.Click
        flag = False
        If flag = False Then

            If state = False Then
                cek = ListView1.SelectedItems.Item(0).Text - 1
                state = True
                gerak = ListView1.Items(cek).SubItems(1).Text
            End If

            '' --- error saat delete , kasih trycacth disini ---------------
            Try
                ListView1.Items(cek).SubItems(1).Text = gerak
            Catch ex As Exception
            End Try
            '----------------------'


            '----------------------'
            'TextBox1.BackColor = BackgroundMoving
            'TextBox1.ForeColor = ForegroundMoving
            'TextBox1.Font = FontMoving
            'ListView1.MultiSelect = True
            Timer1.Enabled = True
            For Each x As ListViewItem In ListView1.SelectedItems
                gerak = x.SubItems(1).Text
                cek = x.Index
            Next
            gerak2 = "     " + gerak + "                                                 "
            stringSebelumnya = gerak
            bolShuf = False
            prev = False
            rev = False

        End If
    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim tampGerak As Char
        Dim array As Char() = gerak2.ToCharArray()
        tampGerak = array(0)
        For j As Integer = 0 To gerak2.Length - 1
            Try
                array(j) = array(j + 1)

            Catch ex As Exception
            End Try
        Next
        array(gerak2.Length - 1) = tampGerak
        gerak2 = New String(array)
        'TextBox1.Text = gerak2
        '----------------------- tambah --------
        Try

            ListView1.Items(cek).SubItems(1).Text = gerak2

        Catch ex As Exception
        End Try


    End Sub

    Private Sub ListView1_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.MouseLeave
        Timer1.Enabled = False
        'TextBox1.Text = ""
        ListView1.MultiSelect = True

    End Sub

    '------------------------------ option ----------------------------------------------------------------------
    'Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim selectedIndex As Integer
    '    selectedIndex = ComboBox1.SelectedIndex

    '    If (selectedIndex = 0) Then
    '        newPlaylist()


    '    ElseIf (selectedIndex = 1) Then
    '        save()
    '    ElseIf (selectedIndex = 2) Then
    '        load()
    '    End If

    'End Sub
    

    Function newPlaylist()
        ListView1.Items.Clear()
        i = 0
    End Function

    Function save()
        If (ListView1.Items.Count = 0) Then
            MsgBox("You can't save it. Playlist is empty !")

        Else

            Dim tampungan As String = ""
            Dim dr As DialogResult
            dr = SaveFileDialog1.ShowDialog()
            For i = 0 To ListView1.Items.Count - 2
                tampungan += ListView1.Items(i).SubItems(4).Text + vbNewLine
            Next
            tampungan += ListView1.Items(i).SubItems(4).Text
            If dr = DialogResult.OK Then
                System.IO.File.WriteAllText(SaveFileDialog1.FileName + ".txt", tampungan)
                namafile = SaveFileDialog1.FileName
            End If
        End If

    End Function

    Function getAll()
        Return ListView1.Items.Count
    End Function

    Function load()
        Dim dr As DialogResult
        Dim tampungan As String = ""
        dr = OpenFileDialog1.ShowDialog()
        If dr = DialogResult.OK Then

            tampungan = System.IO.File.ReadAllText(OpenFileDialog1.FileName)

            newPlaylist()

            Dim result As String() = tampungan.Split(New String() {vbNewLine}, StringSplitOptions.None)
            For Each pathLoad As String In result
                '---------------------------------------------------------------------
                i += 1
                LItem = New ListViewItem()
                CurrentMp3 = New Mp3Class(pathLoad) ' dari class id3v1
                'kalau pake class 1 artis gk baca dg benar mknyA pake class 2
                trak = New mcTrack() 'class id3v2
                trak = mfcReadID3v2Tag.ReadID3v2Tag(pathLoad)

                With LItem
                    .SubItems(0).Text = i.ToString
                    .SubItems.Add(Path.GetFileNameWithoutExtension(pathLoad))
                    .SubItems.Add(trak.Artist)
                    .SubItems.Add(CurrentMp3.GetDurationString)
                    .SubItems.Add(pathLoad)
                End With
                ListView1.Items.Add(LItem)

                '----------------------------------------------------------------------
            Next
        End If

    End Function

    Private Sub ComboBox1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = True
    End Sub

    '---------------------------------- shuffle --------------------------------------------------------------------
    Private Sub butShuffle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butShuffle.Click
        If (ListView1.Items.Count > 0) Then
            shuffle()
        Else
            MsgBox("You can't shuffle it. Playlist is empty !")
        End If

    End Sub


    Function shuffle()
        bolShuf = True
        prev = False
        rev = False
        ListView1.FocusedItem.Focused = False
        Timer1.Enabled = True
        ListView1.MultiSelect = False
        Dim random As New System.Random
        lvIndex = random.Next(0, ListView1.Items.Count - 1)
        ListView1.Items(lvIndex).Selected = True
        ListView1.Focus()
        ListView1.FocusedItem.Focused = True

    End Function

    Function setIndexPrev(value As System.Int32)
        prev = True
        rev = False
        bolShuf = False
        Index = value
        ListView1.MultiSelect = False
        ListView1.Items(value).Selected = True
        ListView1.Focus()
    End Function

    Function setIndexRev(value As System.Int32)
        rev = True
        prev = False
        bolShuf = False
        Index = value
        ListView1.MultiSelect = False
        ListView1.Items(value).Selected = True
        ListView1.Focus()
    End Function

    '------------------------------------- property ------------------------------------------------------------



    Public Property IntervalMove As Integer
        Get
            Return setTimer
        End Get
        Set(ByVal value As Integer)
            setTimer = value

        End Set
    End Property

    Public Property PlaylistSize As formSize
        Get
            Return sizePlaylist
        End Get
        Set(value As formSize)
            If value = formSize.SMALL Then
                small()
            ElseIf value = formSize.MEDIUM Then
                medium()
            Else
                large()
            End If
            sizePlaylist = value
        End Set
    End Property

    ' -------------------- public  method ------------------------
    Public Function getPath(ByVal sender As Integer) As String
        Return ListView1.Items(sender).SubItems(4).Text
    End Function

    Function getIndex()
        'Return ListView1.FocusedItem.Index
        If prev = True Then
            Return Index
        ElseIf rev = True Then
            Return Index
        ElseIf bolShuf = True Then
            Return lvIndex
        Else
            Return ListView1.FocusedItem.Index
        End If

    End Function

    Function getTitle()
        If prev = True Then
            Return ListView1.Items(Index).SubItems(1).Text
        ElseIf rev = True Then
            Return ListView1.Items(Index).SubItems(1).Text
        ElseIf bolShuf = True Then
            Return ListView1.Items(lvIndex).SubItems(1).Text
        Else
            Return ListView1.Items(ListView1.FocusedItem.Index).SubItems(1).Text
        End If
    End Function

    Function getItem()
        If prev = True Then
            Return ListView1.Items(Index).SubItems(4).Text
        ElseIf rev = True Then
            Return ListView1.Items(Index).SubItems(4).Text
        ElseIf bolShuf = True Then
            Return ListView1.Items(lvIndex).SubItems(4).Text
        Else
            Return ListView1.Items(ListView1.FocusedItem.Index).SubItems(4).Text
        End If

    End Function

    'Public ReadOnly Property getDuration(ByVal sender As Integer) As String
    '    Get
    '        Return ListView1.Items(sender).SubItems(3).Text

    '    End Get

    'End Property

    'Public ReadOnly Property getTitle(ByVal sender As Integer) As String
    '    Get
    '        Return ListView1.Items(sender).SubItems(1).Text

    '    End Get

    'End Property

    'Public ReadOnly Property getArtist(ByVal sender As Integer) As String
    '    Get
    '        Return ListView1.Items(sender).SubItems(2).Text
    '    End Get

    'End Property

    Public Function listSize() As Integer

        Return ListView1.Items.Count

    End Function


    'Public Function selectedIndex()

    '    Try

    '    If ListView1.FocusedItem.Index = True Then
    '        Return ListView1.Items(ListView1.FocusedItem.Index).SubItems(0)
    '    Else
    '        Return "Focused item is null"
    '    End If
    '    Catch ex As Exception
    '    End Try

    'End Function


    Function small()
        Me.Size = New Size(300, 250)
        ListView1.Columns(0).Width = 29
        ListView1.Columns(1).Width = 140
        ListView1.Columns(2).Width = 50
        ListView1.Columns(3).Width = 62
        ListView1.Columns(4).Width = 0
        ListView1.Size = New Size(300, 190)
        ListView1.Font = New Font("Lucida Sans Unicode", 7, FontStyle.Bold)

    End Function

    Function medium()
        Me.Size = New Size(400, 345)
        ListView1.Columns(0).Width = 30
        ListView1.Columns(1).Width = 200
        ListView1.Columns(2).Width = 80
        ListView1.Columns(3).Width = 70
        ListView1.Columns(4).Width = 0
        ListView1.Size = New Size(400, 280)
        ListView1.Font = New Font("Lucida Sans Unicode", 8, FontStyle.Bold)

    End Function

    Function large()
        Me.Size = New Size(450, 385)
        ListView1.Columns(0).Width = 30
        ListView1.Columns(1).Width = 220
        ListView1.Columns(2).Width = 100
        ListView1.Columns(3).Width = 80
        ListView1.Columns(4).Width = 0
        ListView1.Size = New Size(450, 320)
        ListView1.Font = New Font("Lucida Sans Unicode", 9, FontStyle.Bold)

    End Function


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        save()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        load()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        newPlaylist()
    End Sub

End Class

